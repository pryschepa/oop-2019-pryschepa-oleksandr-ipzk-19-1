#include <QCoreApplication>
#include <iostream>
#include <list>
#include <QList>
#include <QVector>

using namespace std;

template<typename T>
double MinElem (T Tlist){
    double min = 99999;
    for(int i = 0; i < Tlist.size(); i++){
        if(Tlist[i]<min)
            min = Tlist[i];
    }
    return min;
}

template<typename T>
double MaxElem (T Tlist){
    double max = -99999;
    for(int i = 0; i < Tlist.size(); i++){
        if(Tlist[i]>max)
            max = Tlist[i];
    }
    return max;
}

template<typename T>
double Mean (T Tlist){
    double mean = 0;
    for(int i = 0; i < Tlist.size(); i++){
        mean+=Tlist[i];
    }
    mean = mean/Tlist.size();
    return  mean;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    srand(time(NULL));
    double elem;
    int size;
    QList<double> list;
    QVector<double> vector;
    cout<<"Enter size: ";
    cin>>size;
    for(int i=0;i<size;i++){
        cout<<"Enter elem: ";
        cin>>elem;
        vector.push_back(elem);
        list.push_back(elem);
    }
    cout<<"Max elem: "<<MaxElem(list)<<endl;
    cout<<"Min elem: "<<MinElem(vector)<<endl;
    cout<<"Mean value: "<<Mean(list)<<endl;
    return a.exec();
}
