#include <QCoreApplication>
#include <iostream>
#include <list>
#include <QList>
#include <QVector>


using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    double elem;
    int size;
    QList<double> list;
    QVector<double> vector;
    srand(time(NULL));
    cout<<"Enter size: ";
    cin>>size;
    for(int i=0;i<size;i++){
        elem = rand() % 101;
        vector.push_back(elem);
        list.push_back(elem);
        cout<<vector[i]<<" ";
    }
    cout<<endl;
    int tVect, secVect = 0;
    auto countVectorSec = [size, vector, &tVect, &secVect](){
        for(int i=1;i<size;i++){
            if (vector[i]<vector[i-1])
                tVect=1;
            else{
                if (tVect==1)
                    secVect++;
                tVect=0;
            }
        }
    if(tVect==1){
        secVect++;
    }};
    int tList, secList = 0;
    auto countListSec = [size, list, &tList, &secList](){
        for(int i=1;i<size;i++){
                if (list[i]<list[i-1])
                    tList=1;
                else{
                    if (tList==1)
                        secList++;
                    tList=0;
                }
         }
         if(tList==1){
            secList++;
         }
    };
    countVectorSec();
    countListSec();
    cout<<"Count of vector sections: "<<secVect<<endl<<"Count of list sections: "<<secList;
    return a.exec();
}
