#include <QCoreApplication>
#include <iostream>
#include <list>
#include <QList>
#include <QVector>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    double elem;
    int size;
    QList<double> list;
    QVector<double> vector;
    cout<<"Enter size: ";
    cin>>size;
    for(int i=0;i<size;i++){
        cout<<"Enter elem: ";
        cin>>elem;
        vector.push_back(elem);
        list.push_back(elem);
        cout<<"Capasity: "<<vector.capacity()<<endl;
    }
    sort(vector.begin(), vector.end());
    for(int i=0; i<vector.size(); i++){
        cout<<vector[i]<<" ";
    }
    cout<<endl;
    QVector<double> newlist = list.toVector();
    sort(newlist.begin(), newlist.end());
    list = newlist.toList();
    for(auto i=list.begin(); i!=list.end(); i++){
        cout<<*i<<" ";
    }
    return a.exec();
}
