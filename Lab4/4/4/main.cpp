#include <QCoreApplication>
#include <iostream>
#include <list>
#include <QList>
#include <QVector>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    double elem;
    int size;
    QList<double> list;
    QVector<double> vector;
    srand(time(NULL));
    cout<<"Enter size: ";
    cin>>size;
    for(int i=0;i<size;i++){
        elem = rand() % 101;
        vector.push_back(elem);
        list.push_back(elem);
        cout<<vector[i]<<" ";
    }
    cout<<endl;
    QList<double> list1;
    QVector<double> vector1;
    double count;
    for (int i = 0; i<size; i++){
        count=0;
        for(int j = 0; j<i+1; j++){
            count+=vector[j];
        }
        vector1.push_back(count);
        list1.push_back(count);
        cout<<vector1[i]<<" ";
    }
    cout<<endl;
    return a.exec();
}
