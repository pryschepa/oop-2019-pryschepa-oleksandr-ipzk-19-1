#include <QCoreApplication>
#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    int size;
    cout<<"Enter size: ";
    cin>>size;
    int array [size];
    srand(time(NULL));
    for(int i = 0; i<size; i++){
        array[i]=rand() % 101;
        cout<<array[i]<<" ";
    }
    cout<<endl;
    cout<<"First elem: "<<&array[0]<<endl<<"Last elem: "<<&array[size-1]<<endl;
    for(int i = 0; i<size/2; i++){
        swap(array[i],array[size-i-1]);
    }
    for(int i = 0; i<size; i++){
        cout<<array[i]<<" ";
    }
    return a.exec();
}
