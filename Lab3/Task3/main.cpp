#include <QCoreApplication>
#include <iostream>

using namespace std;

void ShowMatrix(int a[], int size){
    for(int i=0;i<size;i++){
        for(int j=0;j<size;j++){
            cout<<a[i * size + j]<<" ";
        }
        cout<<endl;
    }
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    const int size = 4;
    int matrix [size][size];
    for(int i=0;i<size;i++){
        for(int j=0;j<size;j++){
            matrix[i][j] = rand() % 21;
        }
    }
    ShowMatrix(&matrix[0][0],size);
    int min = 999999;
    int *adress;
    int minArray[size];
    for(int i=0;i<size;i++){
        for(int j=0;j<size;j++){
            if (matrix[i][j]<min){
                min=matrix[i][j];
                adress=&matrix[i][j];
            }
        }
        cout<<adress<<endl;
        minArray[i]=min;
        min=999999;
    }
    for(int i=0;i<size;i++){
        cout<<minArray[i]<<" ";
    }
    return a.exec();
}
