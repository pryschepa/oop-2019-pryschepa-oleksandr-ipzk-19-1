#include <QCoreApplication>
#include <iostream>

using namespace std;

void ShowArray(int *arr, int size){
    for(int i=0;i<size;i++){
        cout<<arr[i]<<" ";
    }
    cout<<endl;
}

void DeleteLastZero(int *&arr, int &size){
    int lastZero;
    bool zero = false;
    for (int i=0;i<size;i++){
        if(arr[i]==0){
            lastZero = i;
            zero = true;
        }
    }
    if (zero == true){
        int *newArray = new int[size-1];
        for(int i=0;i<lastZero;i++){
            newArray[i] = arr[i];
        }
        size--;
        for(int i=lastZero;i<size;i++){
            newArray[i] = arr[i+1];
        }
        delete [] arr;
        arr = newArray;
    }
}

void PushBack(int *&arr, int &size, int index){
    index++;
    int *newArray = new int[size+1];
    for (int i=0;i<index;i++){
        newArray[i] = arr[i];
    }
    newArray[index] = 100;
    size++;
    for(int i=index+1;i<size;i++){
        newArray[i] = arr[i-1];
    }
    delete [] arr;
    arr = newArray;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    int size;
    cout<<"Enter size: ";
    cin>>size;
    int *array = new int[size];
    srand(time(NULL));
    for(int i=0;i<size;i++){
        array[i] = rand() % 6;
    }
    ShowArray(array, size);
    PushBack(array, size, 0);
    ShowArray(array, size);
    DeleteLastZero(array, size);
    ShowArray(array,size);
    delete [] array;
    return a.exec();
}
