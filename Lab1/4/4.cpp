// 4.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include <Windows.h>
#define PI 3.14159265

using namespace std;

void first(float S, float h, float alpha) {
	float c = 2 * S / h;
	float b = h / sin(alpha * PI / 180);
	float a = sqrt(pow(b, 2) + pow(c, 2) - 2 * b * c * cos(alpha * PI / 180));
	float P = a + b + c;
	float beta = acos((pow(a, 2) + pow(c, 2) - pow(b, 2)) / (2 * a * c)) * 180 / PI;
	float gamma = acos((pow(a, 2) + pow(b, 2) - pow(c, 2)) / (2 * a * b)) * 180 / PI;
	cout << "Сторона a " << a;
	cout << "\nСторона b " << b;
	cout << "\nСторона c " << c;
	cout << "\nВысота " << h;
	cout << "\nПериметр " << P;
	cout << "\nПлощадь " << S;
	cout << "\nУгол альфа " << alpha;
	cout << "\nУгол бета " << beta;
	cout << "\nУгол гамма " << gamma << "\n";
}

void second(float a, float b, float P) {
	float c = P - (a + b);
	float p = P / 2;
	float S = sqrt(p*(p - a)*(p - b)*(p - c));
	float h = 2 * S / c;
	float alpha = acos((pow(b, 2) + pow(c, 2) - pow(a, 2)) / (2 * c * b)) * 180 / PI;
	float beta = acos((pow(a, 2) + pow(c, 2) - pow(b, 2)) / (2 * a * c)) * 180 / PI;
	float gamma = acos((pow(a, 2) + pow(b, 2) - pow(c, 2)) / (2 * a * b)) * 180 / PI;
	cout << "Сторона a " << a;
	cout << "\nСторона b " << b;
	cout << "\nСторона c " << c;
	cout << "\nВысота " << h;
	cout << "\nПериметр " << P;
	cout << "\nПлощадь " << S;
	cout << "\nУгол альфа " << alpha;
	cout << "\nУгол бета " << beta;
	cout << "\nУгол гамма " << gamma << "\n";
}

void third(float S, float c, float alpha) {
	float h = 2 * S / c;
	float b = h / sin(alpha * PI / 180);
	float a = sqrt(pow(b, 2) + pow(c, 2) - 2 * b*c*cos(alpha * PI / 180));
	float P = a + b + c;
	float beta = acos((pow(a, 2) + pow(c, 2) - pow(b, 2)) / (2 * a * c)) * 180 / PI;
	float gamma = acos((pow(a, 2) + pow(b, 2) - pow(c, 2)) / (2 * a * b)) * 180 / PI;
	cout << "Сторона a " << a;
	cout << "\nСторона b " << b;
	cout << "\nСторона c " << c;
	cout << "\nВысота " << h;
	cout << "\nПериметр " << P;
	cout << "\nПлощадь " << S;
	cout << "\nУгол альфа " << alpha;
	cout << "\nУгол бета " << beta;
	cout << "\nУгол гамма " << gamma << "\n";
}

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	first(6.495, 2.598, 60);
	cout << "*************************\n";
	second(4.359, 3, 12.359);
	cout << "*************************\n";
	third(6.495, 5, 60);
	system("pause");
	return 0;

}


