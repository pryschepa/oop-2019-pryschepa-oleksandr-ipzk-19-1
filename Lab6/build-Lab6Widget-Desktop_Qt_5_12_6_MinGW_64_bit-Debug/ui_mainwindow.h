/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QLineEdit *lineEdit;
    QLineEdit *lineEdit2;
    QLineEdit *lineEdit3;
    QLineEdit *lineEdit4;
    QLineEdit *lineEdit5;
    QLineEdit *lineEdit6;
    QLineEdit *lineEdit7;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout_3;
    QLabel *label;
    QLabel *label2;
    QLabel *label3;
    QLabel *label4;
    QLabel *label5;
    QLabel *label6;
    QLabel *label7;
    QPushButton *pushButton;
    QLineEdit *lineEdit_2;
    QLabel *label_2;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(800, 600);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayoutWidget = new QWidget(centralwidget);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(369, 30, 191, 331));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        lineEdit = new QLineEdit(verticalLayoutWidget);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setReadOnly(true);

        verticalLayout->addWidget(lineEdit);

        lineEdit2 = new QLineEdit(verticalLayoutWidget);
        lineEdit2->setObjectName(QString::fromUtf8("lineEdit2"));
        lineEdit2->setReadOnly(true);

        verticalLayout->addWidget(lineEdit2);

        lineEdit3 = new QLineEdit(verticalLayoutWidget);
        lineEdit3->setObjectName(QString::fromUtf8("lineEdit3"));
        lineEdit3->setReadOnly(true);

        verticalLayout->addWidget(lineEdit3);

        lineEdit4 = new QLineEdit(verticalLayoutWidget);
        lineEdit4->setObjectName(QString::fromUtf8("lineEdit4"));
        lineEdit4->setReadOnly(true);

        verticalLayout->addWidget(lineEdit4);

        lineEdit5 = new QLineEdit(verticalLayoutWidget);
        lineEdit5->setObjectName(QString::fromUtf8("lineEdit5"));
        lineEdit5->setReadOnly(true);

        verticalLayout->addWidget(lineEdit5);

        lineEdit6 = new QLineEdit(verticalLayoutWidget);
        lineEdit6->setObjectName(QString::fromUtf8("lineEdit6"));
        lineEdit6->setReadOnly(true);

        verticalLayout->addWidget(lineEdit6);

        lineEdit7 = new QLineEdit(verticalLayoutWidget);
        lineEdit7->setObjectName(QString::fromUtf8("lineEdit7"));
        lineEdit7->setReadOnly(true);

        verticalLayout->addWidget(lineEdit7);

        verticalLayoutWidget_2 = new QWidget(centralwidget);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(110, 30, 160, 331));
        verticalLayout_3 = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(verticalLayoutWidget_2);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout_3->addWidget(label);

        label2 = new QLabel(verticalLayoutWidget_2);
        label2->setObjectName(QString::fromUtf8("label2"));

        verticalLayout_3->addWidget(label2);

        label3 = new QLabel(verticalLayoutWidget_2);
        label3->setObjectName(QString::fromUtf8("label3"));

        verticalLayout_3->addWidget(label3);

        label4 = new QLabel(verticalLayoutWidget_2);
        label4->setObjectName(QString::fromUtf8("label4"));

        verticalLayout_3->addWidget(label4);

        label5 = new QLabel(verticalLayoutWidget_2);
        label5->setObjectName(QString::fromUtf8("label5"));

        verticalLayout_3->addWidget(label5);

        label6 = new QLabel(verticalLayoutWidget_2);
        label6->setObjectName(QString::fromUtf8("label6"));

        verticalLayout_3->addWidget(label6);

        label7 = new QLabel(verticalLayoutWidget_2);
        label7->setObjectName(QString::fromUtf8("label7"));

        verticalLayout_3->addWidget(label7);

        pushButton = new QPushButton(centralwidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(230, 400, 80, 25));
        lineEdit_2 = new QLineEdit(centralwidget);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(340, 400, 31, 24));
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(390, 400, 101, 16));
        QFont font;
        font.setPointSize(10);
        label_2->setFont(font);
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 25));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        label->setText(QApplication::translate("MainWindow", "Name", nullptr));
        label2->setText(QApplication::translate("MainWindow", "Price", nullptr));
        label3->setText(QApplication::translate("MainWindow", "Name of cost", nullptr));
        label4->setText(QApplication::translate("MainWindow", "Ex rate", nullptr));
        label5->setText(QApplication::translate("MainWindow", "Quantity", nullptr));
        label6->setText(QApplication::translate("MainWindow", "Producer", nullptr));
        label7->setText(QApplication::translate("MainWindow", "Weight", nullptr));
        pushButton->setText(QApplication::translate("MainWindow", "\320\222\321\213\320\262\320\265\321\201\321\202\320\270", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "\320\255\320\273\320\265\320\274\320\265\320\275\321\202", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
