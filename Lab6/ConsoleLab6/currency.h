#ifndef CURRENCY_H
#define CURRENCY_H
#include <QString>

class Currency
{
private:
    QString Name;
    double ExRate;
public:
    Currency();
    Currency(double exRate);
    Currency(QString name, double exRate);
    Currency(Currency &obj);

    QString GetName();
    void SetName(QString name);
    double GetExRate();
    void SetExRate(double exRate);
};

#endif // CURRENCY_H
