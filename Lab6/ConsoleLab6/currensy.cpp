#include "currency.h"

Currency::Currency()
{
    Name = "UAH";
    ExRate = 1;
}
Currency::Currency(double exRate)
{
    Name = "No Name";
    ExRate = exRate;
}
Currency::Currency(QString name, double exRate)
{
    Name = name;
    ExRate = exRate;
}
Currency::Currency(Currency &obj)
{
    Name = obj.Name;
    ExRate = obj.ExRate;
}

QString Currency::GetName()
{
    return Name;
}
void Currency::SetName(QString name)
{
    Name = name;
}
double Currency::GetExRate()
{
    return ExRate;
}
void Currency::SetExRate(double exRate)
{
    ExRate = exRate;
}
