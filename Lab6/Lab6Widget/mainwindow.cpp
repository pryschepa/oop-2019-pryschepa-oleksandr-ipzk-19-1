#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QCoreApplication>
#include <QTextStream>
#include <product.h>
#include <QVector>
#include <QString>
#include <iostream>
#include <algorithm>
#include <string>
#include <sstream>
#include <cstdlib>

Product*ReadProductArray(int count);
void PrintProduct(Product obj);
void PrintProducts(Product *products);
void GetProductsInfo(Product products[], Product &chipest, Product &richest);
void SortProductsByPrice(Product *products, int size);
void SortProductsByCount(Product *products, int size);

QTextStream cin(stdin);

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

Product* ReadProductArray(int count)
{
    QString Name;
    double Price;
    QString NameOfCost;
    double ExRate;
    int Quantity;
    QString Producer;
    int Weight;
    Product Products[count];
    for (int i = 0; i < count; i++){
            std::cout<<"Enter info about product\n";
            std::cout<<"Enter name: ";
            Name = cin.readLine();
            std::cout<<"Enter price: ";
            cin>>Price;
            std::cout<<"Enter name of cost: ";
            cin.flush();
            NameOfCost = cin.readLine();
            std::cout<<"Enter ex rate: ";
            cin>>ExRate;
            Currency currency(NameOfCost, ExRate);
            std::cout<<"Enter quantity: ";
            cin>>Quantity;
            std::cout<<"Enter producer: ";
            Producer = cin.readLine();
            std::cout<<"Enter weight: ";
            cin>>Weight;
            Product product(Name, Price, currency, Quantity, Producer, Weight);
            Products[i] = Product(product);
    }
    return Products;
}

void PrintProduct(Product obj)
{
    std::cout<<"Info about product\n";
    std::cout<<"Name: "<<obj.GetName().toStdString()<<std::endl;
    std::cout<<"Price: "<<obj.GetPrice()<<std::endl;
    std::cout<<"Name of cost: "<<obj.GetCost().GetName().toStdString()<<std::endl;
    std::cout<<"Ex rate: "<<obj.GetCost().GetExRate()<<std::endl;
    std::cout<<"Quantity: "<<obj.GetQuantity()<<std::endl;
    std::cout<<"Producer: "<<obj.GetProducer().toStdString()<<std::endl;
    std::cout<<"Weight: "<<obj.GetWeight()<<std::endl;
}

void PrintProducts(Product *products)
{
    for (int i = 0; i<(sizeof (&products) / sizeof (&products[0]))+1; i++){
        std::cout<<"Info about product "<<i+1<<std::endl;
        std::cout<<"Name: "<<products[i].GetName().toStdString()<<std::endl;
        std::cout<<"Price: "<<products[i].GetPrice()<<std::endl;
        std::cout<<"Name of cost: "<<products[i].GetCost().GetName().toStdString()<<std::endl;
        std::cout<<"Ex rate: "<<products[i].GetCost().GetExRate()<<std::endl;
        std::cout<<"Quantity: "<<products[i].GetQuantity()<<std::endl;
        std::cout<<"Producer: "<<products[i].GetProducer().toStdString()<<std::endl;
        std::cout<<"Weight: "<<products[i].GetWeight()<<std::endl<<std::endl;
    }
}
void GetProductsInfo(Product *products, Product &chipest, Product &richest)
{
    double min = INT_MAX;
    double max = INT_MIN;
    for (int i = 0; i<(sizeof (&products) / sizeof (&products[0]))+1; i++){
        if (products[i].GetPriceInUAH()>max)
            richest = products[i];
        if (products[i].GetPriceInUAH()<min)
            chipest = products[i];
    }
}

void SortProductsByPrice(Product *products, int size)
{
    Product temp;
    for (int i = 0; i < size - 1; i++) {
        for (int j = 0; j < size - i - 1; j++) {
            if (products[j].GetPriceInUAH() > products[j + 1].GetPriceInUAH()) {
                temp = products[j];
                products[j] = products[j + 1];
                products[j + 1] = temp;
            }
        }
    }
}

void SortProductsByCount(Product *products, int size)
{
    Product temp;
    for (int i = 0; i < size - 1; i++) {
        for (int j = 0; j < size - i - 1; j++) {
            if (products[j].GetQuantity() > products[j + 1].GetQuantity()) {

                temp = products[j];
                products[j] = products[j + 1];
                products[j + 1] = temp;
            }
        }
    }
}

void MainWindow::on_pushButton_clicked()
{

}
