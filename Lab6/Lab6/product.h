#ifndef PRODUCT_H
#define PRODUCT_H
#include <QString>
#include "currency.h"

class Product
{
private:
    QString Name;
    double Price;
    Currency Cost;
    int Quantity;
    QString Producer;
    int Weight;
public:
    Product();
    Product(QString name, double price);
    Product(QString name, double price, Currency cost, int quantity, QString producer, int weight);
    Product(Product &obj);

    QString GetName();
    void SetName(QString name);
    double GetPrice();
    void SetPrice(double price);
    Currency GetCost();
    void SetCost(Currency cost);
    int GetQuantity();
    void SetQuantity(int quantity);
    QString GetProducer();
    void SetProducer(QString producer);
    int GetWeight();
    void SetWeight(int weight);

    double GetPriceInUAH();
    double GetTotalPriceInUAH();
    int GetTotalWeight();
};

#endif // PRODUCT_H
