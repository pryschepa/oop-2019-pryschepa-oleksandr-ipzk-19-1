#include "product.h"

Product::Product()
{
    Name = "No name";
    Price = 1;
    Cost = Currency();
    Quantity = 1;
    Producer = "No producer";
    Weight = 1;
}
Product::Product(QString name, double price)
{
    Name = name;
    Price = price;
    Cost = Currency();
    Quantity = 1;
    Producer = "No producer";
    Weight = 1;
}
Product::Product(QString name, double price, Currency cost, int quantity, QString producer, int weight)
{
    Name = name;
    Price = price;
    Cost = Currency(cost);
    Quantity = quantity;
    Producer = producer;
    Weight = weight;
}
Product::Product(Product &obj)
{
    Name = obj.Name;
    Price = obj.Price;
    Cost = Currency(obj.Cost);
    Quantity = obj.Quantity;
    Producer = obj.Producer;
    Weight = obj.Weight;
}

QString Product::GetName()
{
    return Name;
}
void Product::SetName(QString name)
{
    Name = name;
}
double Product::GetPrice()
{
    return Price;
}
void Product::SetPrice(double price)
{
    Price = price;
}
Currency Product::GetCost()
{
    return Cost;
}
void Product::SetCost(Currency cost)
{
    Cost = Currency(cost);
}
int Product::GetQuantity()
{
    return Quantity;
}
void Product::SetQuantity(int quantity)
{
    Quantity = quantity;
}
QString Product::GetProducer()
{
    return Producer;
}
void Product::SetProducer(QString producer)
{
    Producer = producer;
}
int Product::GetWeight()
{
    return Weight;
}
void Product::SetWeight(int weight)
{
    Weight = weight;
}

double Product::GetPriceInUAH()
{
    return Price*Cost.GetExRate();
}
double Product::GetTotalPriceInUAH()
{
    return Quantity*Price*Cost.GetExRate();
}
int Product::GetTotalWeight()
{
    return Quantity*Weight;
}
