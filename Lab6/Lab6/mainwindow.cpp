#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <product.h>
#include <currency.h>
#include <QVector>
#include <iostream>
#include <QTextStream>
#include <limits.h>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

