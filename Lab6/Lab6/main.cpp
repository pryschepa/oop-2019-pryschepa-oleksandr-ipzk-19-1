#include "mainwindow.h"
#include <product.h>
#include <QApplication>
#include <QTextStream>

QTextStream cout(stdout);
QTextStream cin(stdin);

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
QVector<Product> ReadProductArray(int count)
{
    QString Name;
    double Price;
    QString NameOfCost;
    double ExRate;
    int Quantity;
    QString Producer;
    int Weight;
    QVector<Product> Products;
    for (int i = 0; i < count; i++){
            cout<<"Enter info about product/n";
            cout<<"Enter name: ";
            Name = cin.readLine();
            cout<<"Enter price: ";
            cin>>Price;
            cout<<"Enter name of cost: ";
            NameOfCost = cin.readLine();
            cout<<"Enter ex rate: ";
            cin>>ExRate;
            Currency currency(NameOfCost, ExRate);
            cout<<"Enter quantity: ";
            cin>>Quantity;
            cout<<"Enter producer: ";
            Producer = cin.readLine();
            cout<<"Enter weight: ";
            cin>>Weight;
            Product product(Name, Price, currency, Quantity, Producer, Weight);
            Products.push_back(product);
    }
    return Products;
}

void PrintProduct(Product obj)
{
    cout<<"Info about product/n";
    cout<<"Name: "<<obj.GetName()<<endl;
    cout<<"Price: "<<obj.GetPrice()<<endl;
    cout<<"Name of cost: "<<obj.GetCost().GetName()<<endl;
    cout<<"Ex rate: "<<obj.GetCost().GetExRate()<<endl;
    cout<<"Quantity: "<<obj.GetQuantity()<<endl;
    cout<<"Producer: "<<obj.GetProducer()<<endl;
    cout<<"Weight: "<<obj.GetWeight()<<endl;
}

void PrintProducts(QVector<Product> products)
{
    for (int i = 0; i<products.size(); i++){
        cout<<"Info about product "<<i+1<<endl;
        cout<<"Name: "<<products[i].GetName()<<endl;
        cout<<"Price: "<<products[i].GetPrice()<<endl;
        cout<<"Name of cost: "<<products[i].GetCost().GetName()<<endl;
        cout<<"Ex rate: "<<products[i].GetCost().GetExRate()<<endl;
        cout<<"Quantity: "<<products[i].GetQuantity()<<endl;
        cout<<"Producer: "<<products[i].GetProducer()<<endl;
        cout<<"Weight: "<<products[i].GetWeight()<<endl;
    }
}
void GetProductsInfo(QVector<Product> products, Product &chipest, Product &richest)
{
    double min = INT_MAX;
    double max = INT_MIN;
    for (int i = 0; i<products.size(); i++){
        if (products[i].GetPriceInUAH()>max)
            richest = products[i];
        if (products[i].GetPriceInUAH()<min)
            chipest = products[i];
    }
}
void SortProducsByPrice(QVector<Product> products)
{
    std::sort(products.begin(), products.end(), [] (Product lh, Product rh)
        {return lh.GetPriceInUAH() < rh.GetPriceInUAH();});
}
void SortProductsByCount(QVector<Product> products)
{
    std::sort(products.begin(), products.end(), [] (Product lh, Product rh)
        {return lh.GetQuantity() < rh.GetQuantity();});
}
