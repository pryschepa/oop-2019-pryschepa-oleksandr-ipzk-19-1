#include <QCoreApplication>
#include <iostream>

using namespace std;

void quickSort(int *numbers, int left, int right)
{
  int pivot; // разрешающий элемент
  int l_hold = left; //левая граница
  int r_hold = right; // правая граница
  pivot = numbers[left];
  while (left < right) // пока границы не сомкнутся
  {
    while ((numbers[right] >= pivot) && (left < right))
      right--; // сдвигаем правую границу пока элемент [right] больше [pivot]
    if (left != right) // если границы не сомкнулись
    {
      numbers[left] = numbers[right]; // перемещаем элемент [right] на место разрешающего
      left++; // сдвигаем левую границу вправо
    }
    while ((numbers[left] <= pivot) && (left < right))
      left++; // сдвигаем левую границу пока элемент [left] меньше [pivot]
    if (left != right) // если границы не сомкнулись
    {
      numbers[right] = numbers[left]; // перемещаем элемент [left] на место [right]
      right--; // сдвигаем правую границу вправо
    }
  }
  numbers[left] = pivot; // ставим разрешающий элемент на место
  pivot = left;
  left = l_hold;
  right = r_hold;
  if (left < pivot) // Рекурсивно вызываем сортировку для левой и правой части массива
    quickSort(numbers, left, pivot - 1);
  if (right > pivot)
    quickSort(numbers, pivot + 1, right);
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    int size;
    cout<<"Enter size"<<endl;
    cin>>size;
    int matrix[size][size];
    srand(time(NULL));
    for(int i=0;i<size;i++){
        for(int j=0;j<size;j++){
            matrix[i][j] = rand() % 101;
            cout<<matrix[i][j]<<" ";
        }
        cout<<endl;
    }
    cout<<endl;
    int col[size][size];
    for(int i=0;i<size;i++){
        for(int j=1;j<size;j+=2){
                col[j][i]=matrix[i][j];
        }
    }
    for(int i=1;i<size;i+=2){
        quickSort(col[i],0,size-1);
    }
    for(int i=0;i<size;i++){
        for(int j=1;j<size;j+=2){
                matrix[i][j]=col[j][i];
        }
    }
    for(int i=0;i<size;i++){
        for(int j=0;j<size;j++){
            cout<<matrix[i][j]<<" ";
        }
        cout<<endl;
    }
    return a.exec();
}

