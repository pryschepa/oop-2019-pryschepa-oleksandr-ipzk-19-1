#include <QCoreApplication>
#include <iostream>
#include <string>
#include <windows.h>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    string dictionary[] = {"Apple", "Butter", "City", "Drawing", "Earth", "Flower", "Git", "House", "Ice", "Juice", "Key", "Lamp", "Mother", "Note", "Octopus", "Pitbull", "Quest", "Rocket", "Star", "Television", "Umbro", "Valve", "Witcher", "Xiaomi", "Yoox", "Zippo"};
    cout<<"Enter letter\n";
    char letter;
    cin>>letter;
    if(letter == 'a' || letter == 'A')
        cout<<dictionary[0];
    else if(letter == 'b' || letter == 'B')
        cout<<dictionary[1];
    else if(letter == 'c' || letter == 'C')
        cout<<dictionary[2];
    else if(letter == 'd' || letter == 'D')
        cout<<dictionary[3];
    else if(letter == 'e' || letter == 'E')
        cout<<dictionary[4];
    else if(letter == 'f' || letter == 'F')
        cout<<dictionary[5];
    else if(letter == 'g' || letter == 'G')
        cout<<dictionary[6];
    else if(letter == 'h' || letter == 'H')
        cout<<dictionary[7];
    else if(letter == 'i' || letter == 'I')
        cout<<dictionary[8];
    else if(letter == 'j' || letter == 'J')
        cout<<dictionary[9];
    else if(letter == 'k' || letter == 'K')
        cout<<dictionary[10];
    else if(letter == 'l' || letter == 'L')
        cout<<dictionary[11];
    else if(letter == 'm' || letter == 'M')
        cout<<dictionary[12];
    else if(letter == 'n' || letter == 'N')
        cout<<dictionary[13];
    else if(letter == 'o' || letter == 'O')
        cout<<dictionary[14];
    else if(letter == 'p' || letter == 'P')
        cout<<dictionary[15];
    else if(letter == 'q' || letter == 'Q')
        cout<<dictionary[16];
    else if(letter == 'r' || letter == 'R')
        cout<<dictionary[17];
    else if(letter == 's' || letter == 'S')
        cout<<dictionary[18];
    else if(letter == 't' || letter == 'T')
        cout<<dictionary[19];
    else if(letter == 'u' || letter == 'U')
        cout<<dictionary[20];
    else if(letter == 'v' || letter == 'V')
        cout<<dictionary[21];
    else if(letter == 'w' || letter == 'W')
        cout<<dictionary[22];
    else if(letter == 'x' || letter == 'X')
        cout<<dictionary[23];
    else if(letter == 'y' || letter == 'Y')
        cout<<dictionary[24];
    else if(letter == 'z' || letter == 'Z')
        cout<<dictionary[25];
    else{
        cout<<"Wrong symbol";
    }
    cout<<"\nSwitch:\n";
    switch (letter) {
        case 'a':
        case 'A': cout<<dictionary[0];break;
        case 'b':
        case 'B': cout<<dictionary[1];break;
        case 'c':
        case 'C': cout<<dictionary[2];break;
        case 'd':
        case 'D': cout<<dictionary[3];break;
        case 'e':
        case 'E': cout<<dictionary[4];break;
        case 'f':
        case 'F': cout<<dictionary[5];break;
        case 'g':
        case 'G': cout<<dictionary[6];break;
        case 'h':
        case 'H': cout<<dictionary[7];break;
        case 'i':
        case 'I': cout<<dictionary[8];break;
        case 'j':
        case 'J': cout<<dictionary[9];break;
        case 'k':
        case 'K': cout<<dictionary[10];break;
        case 'l':
        case 'L': cout<<dictionary[11];break;
        case 'm':
        case 'M': cout<<dictionary[12];break;
        case 'n':
        case 'N': cout<<dictionary[13];break;
        case 'o':
        case 'O': cout<<dictionary[14];break;
        case 'p':
        case 'P': cout<<dictionary[15];break;
        case 'q':
        case 'Q': cout<<dictionary[16];break;
        case 'r':
        case 'R': cout<<dictionary[17];break;
        case 's':
        case 'S': cout<<dictionary[18];break;
        case 't':
        case 'T': cout<<dictionary[19];break;
        case 'u':
        case 'U': cout<<dictionary[20];break;
        case 'v':
        case 'V': cout<<dictionary[21];break;
        case 'w':
        case 'W': cout<<dictionary[22];break;
        case 'x':
        case 'X': cout<<dictionary[23];break;
        case 'y':
        case 'Y': cout<<dictionary[24];break;
        case 'z':
        case 'Z': cout<<dictionary[25];break;
        default: cout<<"Wrong symbol";break;
    }
    return a.exec();
}
