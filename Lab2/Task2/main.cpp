#include <QCoreApplication>
#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    int size, t, sec=0;
    srand(time(NULL));
    cout<<"Enter size: ";
    cin>>size;
    int arr [size];
    for(int i=0;i<size;i++){
        arr[i] = rand() % 101;
        cout<<arr[i]<<endl;
    }
    for(int i=1;i<size;i++){
        if (arr[i]<arr[i-1])
            t=1;
        else{
            if (t==1)
                sec++;
            t=0;
        }
    }
    if(t==1){
        sec++;
    }
    cout<<"Count of sections: "<<sec<<endl;
    return a.exec();
}
