#include <QCoreApplication>
#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    int size;
    srand(time(NULL));
    cout<<"Enter size: ";
    cin>>size;
    int arr1 [size];
    int arr2 [size];
    for(int i=0;i<size;i++){
        arr1[i] = rand() % 101;
        cout<<arr1[i]<<endl;
    }
    cout<<"Second array"<<endl;
    for(int i=0;i<size;i++){
        arr2[i]=0;
        for(int j=0;j<i+1;j++){
            arr2[i]+=arr1[j];
        }
        cout<<arr2[i]<<endl;
    }
    return a.exec();
}
