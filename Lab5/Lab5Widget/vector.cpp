#include "vector.h"
#include <math.h>
#include <iostream>

Vector::Vector()
{
    x = 0;
    y = 0;
    angle = 0;
    abs = 0;
    count++;
}
Vector::Vector(int x, int y)
{
    this->x = x;
    this->y = y;
    angle = 0;
    count++;
}
Vector::Vector(int x, int y, int angle)
{
    this->x = x;
    this->y = y;
    this->angle = angle;
    count++;
}
Vector::Vector(const Vector &vector)
{
    x = vector.x;
    y = vector.y;
    angle = vector.angle;
}
Vector::~Vector()
{
    count--;
}

Vector::operator double()
{
    abs = sqrt(pow(x,2)+pow(y,2));
    return abs;
}
int Vector::getX()
{
    return x;
}
void Vector::setX(int x)
{
    this->x = x;
}
int Vector::getY()
{
    return y;
}
void Vector::setY(int y)
{
    this->y = y;
}
int Vector::getAngle()
{
    return angle;
}
void Vector::setAngle(int angle)
{
    this->angle = angle;
}

Vector Vector::operator+(Vector obj)
{
    int xSum = x + obj.x;
    int ySum = y + obj.y;
    Vector vector (xSum, ySum);
    return vector;
}

Vector Vector::operator-(Vector obj)
{
    int xDif = x - obj.x;
    int yDif = y - obj.y;
    Vector vector (xDif, yDif);
    return vector;
}
Vector Vector::operator*(Vector obj)
{
    int xMult = x * obj.x;
    int yMult = y * obj.y;
    Vector vector (xMult, yMult);
    return vector;
}
void Vector::Reverse()
{
    x = (-x);
    y = (-y);
}
int Vector::count=0;
