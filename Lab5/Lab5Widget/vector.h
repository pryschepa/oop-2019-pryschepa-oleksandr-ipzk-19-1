#include <math.h>
#ifndef VECTOR_H
#define VECTOR_H


class Vector
{
private:
    int x, y;
    int angle;
    double abs;
    static int count;
public:
    Vector();
    Vector(int x, int y);
    Vector(int x, int y, int angle);
    Vector(const Vector &vector);
    ~Vector();

    operator double();
    int getX();
    void setX(int x);
    int getY();
    void setY(int y);
    int getAngle();
    void setAngle(int angle);

    Vector operator +(Vector obj);
    Vector operator -(Vector obj);
    Vector operator *(Vector obj);
    void Reverse();
};

#endif // VECTOR_H
