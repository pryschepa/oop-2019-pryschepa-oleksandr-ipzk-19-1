#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "vector.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_plusButton_clicked()
{
    Vector vector1(ui->x1->text().toInt(), ui->y1->text().toInt());
    Vector vector2(ui->x2->text().toInt(), ui->y2->text().toInt());
    Vector vectorSum = vector1 + vector2;
    ui->x3->setText(QString::number(vectorSum.getX()));
    ui->y3->setText(QString::number(vectorSum.getY()));
}

void MainWindow::on_minusButton_clicked()
{
    Vector vector1(ui->x1->text().toInt(), ui->y1->text().toInt());
    Vector vector2(ui->x2->text().toInt(), ui->y2->text().toInt());
    Vector vectorDif = vector1 - vector2;
    ui->x3->setText(QString::number(vectorDif.getX()));
    ui->y3->setText(QString::number(vectorDif.getY()));
}

void MainWindow::on_multButton_clicked()
{
    Vector vector1(ui->x1->text().toInt(), ui->y1->text().toInt());
    Vector vector2(ui->x2->text().toInt(), ui->y2->text().toInt());
    Vector vectorMult = vector1 * vector2;
    ui->x3->setText(QString::number(vectorMult.getX()));
    ui->y3->setText(QString::number(vectorMult.getY()));
}

void MainWindow::on_revButton1_clicked()
{
        Vector vector(ui->x1->text().toInt(), ui->y1->text().toInt());
        vector.Reverse();
        ui->x1->setText(QString::number(vector.getX()));
        ui->y1->setText(QString::number(vector.getY()));
}

void MainWindow::on_revButton2_clicked()
{
    Vector vector(ui->x2->text().toInt(), ui->y2->text().toInt());
    vector.Reverse();
    ui->x2->setText(QString::number(vector.getX()));
    ui->y2->setText(QString::number(vector.getY()));
}
