/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *x1;
    QLineEdit *x2;
    QLineEdit *y2;
    QLineEdit *y1;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *plusButton;
    QPushButton *minusButton;
    QPushButton *multButton;
    QPushButton *revButton1;
    QPushButton *revButton2;
    QLineEdit *x3;
    QLineEdit *y3;
    QLabel *label_3;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(354, 288);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMinimumSize(QSize(354, 288));
        MainWindow->setMaximumSize(QSize(354, 288));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        label = new QLabel(centralwidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(30, 40, 47, 13));
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(30, 90, 47, 13));
        x1 = new QLineEdit(centralwidget);
        x1->setObjectName(QString::fromUtf8("x1"));
        x1->setGeometry(QRect(110, 40, 40, 21));
        x2 = new QLineEdit(centralwidget);
        x2->setObjectName(QString::fromUtf8("x2"));
        x2->setGeometry(QRect(110, 90, 40, 21));
        y2 = new QLineEdit(centralwidget);
        y2->setObjectName(QString::fromUtf8("y2"));
        y2->setGeometry(QRect(180, 90, 40, 21));
        y1 = new QLineEdit(centralwidget);
        y1->setObjectName(QString::fromUtf8("y1"));
        y1->setGeometry(QRect(180, 40, 40, 21));
        verticalLayoutWidget = new QWidget(centralwidget);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(280, 20, 41, 111));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        plusButton = new QPushButton(verticalLayoutWidget);
        plusButton->setObjectName(QString::fromUtf8("plusButton"));
        QFont font;
        font.setPointSize(10);
        plusButton->setFont(font);

        verticalLayout->addWidget(plusButton);

        minusButton = new QPushButton(verticalLayoutWidget);
        minusButton->setObjectName(QString::fromUtf8("minusButton"));
        minusButton->setFont(font);

        verticalLayout->addWidget(minusButton);

        multButton = new QPushButton(verticalLayoutWidget);
        multButton->setObjectName(QString::fromUtf8("multButton"));
        multButton->setFont(font);

        verticalLayout->addWidget(multButton);

        revButton1 = new QPushButton(centralwidget);
        revButton1->setObjectName(QString::fromUtf8("revButton1"));
        revButton1->setGeometry(QRect(230, 40, 21, 21));
        QIcon icon;
        icon.addFile(QString::fromUtf8("C:/Users/Admin/Desktop/w512h5121377940301185098streamlinesync.png"), QSize(), QIcon::Normal, QIcon::Off);
        revButton1->setIcon(icon);
        revButton2 = new QPushButton(centralwidget);
        revButton2->setObjectName(QString::fromUtf8("revButton2"));
        revButton2->setGeometry(QRect(230, 90, 21, 21));
        revButton2->setIcon(icon);
        x3 = new QLineEdit(centralwidget);
        x3->setObjectName(QString::fromUtf8("x3"));
        x3->setGeometry(QRect(110, 170, 40, 21));
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        x3->setFont(font1);
        x3->setReadOnly(true);
        y3 = new QLineEdit(centralwidget);
        y3->setObjectName(QString::fromUtf8("y3"));
        y3->setGeometry(QRect(180, 170, 40, 21));
        y3->setFont(font1);
        y3->setReadOnly(true);
        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(30, 170, 61, 16));
        label_3->setFont(font1);
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 354, 20));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        label->setText(QApplication::translate("MainWindow", "\320\222\320\265\320\272\321\202\320\276\321\200 1", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "\320\222\320\265\320\272\321\202\320\276\321\200 2", nullptr));
        x1->setInputMask(QApplication::translate("MainWindow", "999", nullptr));
        x1->setPlaceholderText(QApplication::translate("MainWindow", "X", nullptr));
        x2->setInputMask(QApplication::translate("MainWindow", "999", nullptr));
        x2->setPlaceholderText(QApplication::translate("MainWindow", "X", nullptr));
        y2->setInputMask(QApplication::translate("MainWindow", "999", nullptr));
        y2->setText(QString());
        y2->setPlaceholderText(QApplication::translate("MainWindow", "Y", nullptr));
        y1->setInputMask(QApplication::translate("MainWindow", "999", nullptr));
        y1->setText(QString());
        y1->setPlaceholderText(QApplication::translate("MainWindow", "Y", nullptr));
        plusButton->setText(QApplication::translate("MainWindow", "+", nullptr));
        minusButton->setText(QApplication::translate("MainWindow", "-", nullptr));
        multButton->setText(QApplication::translate("MainWindow", "*", nullptr));
        revButton1->setText(QString());
        revButton2->setText(QString());
        x3->setInputMask(QApplication::translate("MainWindow", "X", nullptr));
        x3->setPlaceholderText(QApplication::translate("MainWindow", "X", nullptr));
        y3->setInputMask(QString());
        y3->setText(QString());
        y3->setPlaceholderText(QApplication::translate("MainWindow", "Y", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "\320\240\320\265\320\267\321\203\320\273\321\214\321\202\320\260\321\202", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
